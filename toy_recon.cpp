/* 
   	This contains Commandline Interface to the Recon
 */
#include "gridFFT.h"

using namespace NDarray;
using namespace std;

int main(int argc, char **argv){
	
	int res = 320;
	
	// This the gridding operator
	gridFFT gridding;
	gridding.read_commandline(argc,argv);
	gridding.precalc_gridding(res,res,res,THREED,THREEDNONCARTESIAN);
	
	
	// This is is a typical raw data size
	Array< float, 3>kx(2*res,10000,1,ColumnMajorArray<3>());
	Array< float, 3>ky(2*res,10000,1,ColumnMajorArray<3>());
	Array< float, 3>kz(2*res,10000,1,ColumnMajorArray<3>());
	Array< float, 3>kw(2*res,10000,1,ColumnMajorArray<3>());
	Array< complex<float>, 4>kdata(2*res,10000,1,32,ColumnMajorArray<4>());
	kdata =complex<float>(1.0,0.0);
	kw =1.0;
	
	// These are actually generated from the data
	Array< complex<float>,4> smaps(res,res,res,32,ColumnMajorArray<4>());
	
	// Assign some values
	for( Array<float,3>::iterator miter=kx.begin(); miter != kx.end(); miter++){
		*miter = rand()%res - res/2;
	}
	
	for( Array<float,3>::iterator miter=ky.begin(); miter != ky.end(); miter++){
		*miter = rand()%res - res/2;
	}
	
	// Assign some values
	for( Array<float,3>::iterator miter=kz.begin(); miter != kz.end(); miter++){
		*miter = rand()%res - res/2;
	}
	
	//
	// For each data set we do this ~18cardiac frames * 5 velocity encodes
	//				
	Array< complex<float>,3>X(res,res,res,ColumnMajorArray<3>());
	X = complex<float>(0.0,0.0);
	for(int coil=0; coil< 32; coil++){
		
		Array< complex<float>,3>kdata_one_coil = kdata(Range::all(),Range::all(),Range::all(),coil);
		Array< complex<float>,3>smap_one_coil = smaps(Range::all(),Range::all(),Range::all(),coil);
		
		// K-space to Image
		gridding.forward(X,smap_one_coil,kdata_one_coil,kx,ky,kz,kw);
	}
				
	ArrayWrite( X,"ToyImage.dat");
	

	return(0);
}


